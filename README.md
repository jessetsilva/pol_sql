### What is this repository for? ###

This repository contains the files for the Poludo QA training SQL classes, including handouts, books, presentations  and scripts. The files will be added as we advance in the course and can be modified or removed as needed without notice.

### How do I get set up? ###

* Just download the files

### Who do I talk to? ###

*jessetsilva@gmail.com